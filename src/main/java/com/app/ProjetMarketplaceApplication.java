package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetMarketplaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetMarketplaceApplication.class, args);
	}

}

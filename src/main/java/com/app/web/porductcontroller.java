package com.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.annotation.AccessType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.dao.ProductRepository;
import com.app.entities.Product;
@RestController
@RequestMapping("/restproduit")
@CrossOrigin

public class porductcontroller {
	@Autowired
	ProductRepository PR;
	@Autowired
	private MongoTemplate PR1;
	@GetMapping("/all")
	 public Iterable<Product> Afficherproduit() {
        return PR.findAll();
    }

	@RequestMapping("/findall/{page}")
	public Page<Product> afficherproduit(@PathVariable("page") int page)  {
		return(PR.findAll(PageRequest.of(page,  3)));
	}
	
	@RequestMapping("/findall/{page}/{size}")
	public Page<Product> afficher(@PathVariable("size") int size)  {
		return(PR.findAll(PageRequest.of(size, 1)));
	}
	
	@GetMapping("/findD/{designation}")
	public Iterable<Product> afficherD(@PathVariable("designation") String designation)  {
		return(PR.findBydesignation(designation));
	}
	
	@GetMapping("/findMc")
	public Iterable<Product> afficherMc(String des)  {
		return(PR.findBydesignationContaining(des));
	}
	
	@PostMapping("/Add")
	public Product addproduit(@RequestBody Product P)
	{
		return PR.save(P);
		
	}
	@PutMapping("/update/{Id}")
	public Product update(@PathVariable("Id") String id  , @RequestBody Product P)
	{	P.setId(id);
		return PR.save(P);
	}
	@DeleteMapping("/delete/{Id}")
	   void delete(@PathVariable ("Id") String Id) {	        
	        	PR.deleteById(Id);  
	    }
	@RequestMapping("/price")
	 public Page<Product> RecherheByPrice(double price1, double price2, PageRequest pageRequest){
	     return PR.findByPriceBetween(price1, price2, pageRequest);
	 }
	@GetMapping("/pricemax")
	public List<Product> PmaxPrice(){

		Query Q1= new Query();

		Q1.with(new Sort(Sort.Direction.DESC, "price"));

		Q1.limit(1);

		double P=PR1.findOne(Q1,Product.class).getPrice();
		Query Q2= new Query();

		Q2.addCriteria(Criteria.where("Price").is(P));

		return PR1.find(Q2, Product.class);

		}
	@GetMapping("/pricet")
	public float Totalprix(){
		GroupOperation gr= Aggregation.group().sum("price").as("Total");

		ProjectionOperation po=Aggregation.project("Total");

		AggregationResults<TotalP> result=
		PR1.aggregate(Aggregation.newAggregation(gr,po), Product.class,
		TotalP.class);

		return result.getUniqueMappedResult().Total;
		}
	@GetMapping("/pricemc")
	public float Totalproduitmc(String mc){

		MatchOperation Q1=
		Aggregation.match(Criteria.where("designation").regex(mc));

		GroupOperation gr=
		Aggregation.group().sum("price").as("Total");

		ProjectionOperation po= Aggregation.project("Total");
		AggregationResults<TotalP> result=
		PR1.aggregate(Aggregation.newAggregation(Q1,gr,po),
		Product.class, TotalP.class);

		return result.getUniqueMappedResult().Total;
	}
	public List<Pricehistory> productandhp(){
		GroupOperation gr= Aggregation.group("id").avg("priceHistory.price").as("Moyenne");

		ProjectionOperation
		pro=Aggregation.project("id","Moyenne");

		SortOperation
		sort=Aggregation.sort(Sort.Direction.DESC,"Moyenne");
		AggregationResults<Pricehistory> result=
		PR1.aggregate(Aggregation.newAggregation(gr,pro,sort),
		Product.class, Pricehistory.class);

		return result.getMappedResults();
	}
	public List<Pricehistory> maxproductandhp(){
		MatchOperation
		Match=Aggregation.match(Criteria.where("Moyenne").
		is( productandhp().get(0).getMoyenne()));

		GroupOperation gr=
		Aggregation.group("id").avg("priceHistory.price").
		as("Moyenne");

		ProjectionOperation
		pro=Aggregation.project("id","Moyenne");

		AggregationResults<Pricehistory> result=
		PR1.aggregate(Aggregation.newAggregation(Match,gr,pro),
		Product.class, Pricehistory.class);
		return result.getMappedResults();
	}

		}
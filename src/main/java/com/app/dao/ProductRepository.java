package com.app.dao;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.app.entities.Product;

public interface ProductRepository  extends PagingAndSortingRepository<Product, String> {

	List<Product>findBydesignation(String designation);
	
	
	List<Product>findBydesignationContaining(String designation);
	Page<Product> findByPriceBetween(double price1, double price2 ,PageRequest PageRequest);


}